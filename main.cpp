//
//  main.cpp
//  2D_QDM
//
//  Created by liyuezhen on 2019/12/18.
//  Copyright © 2019 liyuezhen. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <random>
#include <fstream>
using std::cout;
using std::cin;
using std::endl;
using namespace std;

int main(void){
    ofstream myfile;
    if (!myfile){
       //cout << "sth wrong" << endl;
    }
    myfile.open ("3D_staggered_L=6,beta=20,V=0.9.txt", ios::out);
    int N = 4; // system size
    int p, q = 0;
    double prob;
    int Nb = 3 * N * N * N;
    int i[Nb], j[Nb], k[Nb], l[Nb]; // the bond surrounding each plaquette.
    
    std::random_device rd;
    std::mt19937 generate = std::mt19937(314159); // remember to switch back to rd()
    std::uniform_real_distribution<double> conti(0,1);
    
    // Defining which link belongs to each plaquettes. In the content below, ijkl are ordered by their values from small to large.
    
    for (int p = 0; p < N * N * N; p ++){ // plaquettes on the x-y plane
        i[p] = p;
        if ((p % (N * N)) / N == N - 1){
            j[p] = i[p] - N * (N - 1);
        }
        else{
            j[p] = i[p] + N;
        }
        k[p] = p + N * N * N;
        if (p % N == N - 1){
            l[p] = k[p] - N + 1;
        }
        else{
            l[p] = k[p] + 1;
        }
       //cout << i[p] << " " << j[p] << " " << k[p] << " "  << l[p] << endl;
    }
    
    for (int p = N * N * N; p < 2 * N * N * N; p ++){ // x-z plane
        i[p] = p - N * N * N;
        if (i[p] / (N * N) == N - 1){
            j[p] = i[p] - (N - 1) * N * N;
        }
        else{
            j[p] = i[p] + N * N;
        }
        k[p] = p + N * N * N;
        if (k[p] % N == N - 1){
            l[p] = k[p] - N + 1;
        }
        else{
            l[p] = k[p] + 1;
        }
       //cout << i[p] << " " << j[p] << " " << k[p] << " "  << l[p] << endl;
    }
    
    for (int p = 2 * N * N * N; p < 3 * N * N * N; p ++){ // y-z plane
        i[p] = p - N * N * N;
        if(i[p] >= 2 * N * N * N - N * N){
            j[p] = i[p] - (N - 1) * N * N;
        }
        else{
            j[p] = i[p] + N * N;
        }
        k[p] = p;
        if((p % (N * N)) / N == N - 1){
            l[p] = k[p] - (N - 1) * N;
        }
        else{
            l[p] = k[p] + N;
        }
       //cout << i[p] << " " << j[p] << " " << k[p] << " "  << l[p] << endl;
    }

    
    // Variables declaration
    double beta = 20, V = 0.6;
    int num_FP;
    int updating_line[Nb]; // a list recording if there's an updating line at a given link (1 = yes, 0 = no)
    int num_updating_line;
    int M, b, n; // n: number of operators
    int M_max = 50000;
    int s[M_max];
    int s_new[M_max];
    
    int link[Nb];
    int link_new[Nb];
    int link0[Nb];
    int link0new[Nb];
    int MC_step = 100;
    int num_FP_after;
    int a = 0; // to determine if we have an acceptable off-diagonal update
    int u; // just some dummy variable
    int psi_x = 0, psi_y = 0, psi_z = 0;
    double avg_x = 0, avg_y = 0, avg_z = 0, avg_r = 0;
    double xy = 0, xz = 0, yz = 0, Cos, r;
    int phase[Nb];
    
    
    num_FP = 0;
    num_updating_line = 0;
    num_FP_after = 0;
    n = 0;
    
    M = 170;
    for (p = 0; p < M_max; p ++){
        s[p] = -1;
        s_new[p] = -1;
    }
    
    myfile << "x y z r" << endl;
    
    // Let's just start from the columnar state along the x-direction
    for (int i = 0; i < Nb; i ++){
        if (i % 2 == 0 && i < Nb / 3){
            link[i] = 1;
        }
        else{
            link[i] = 0;
        }
       //cout << link[i] << " ";
    }
   //cout << endl;
    
    // define the phase factor
    
    // columnar
    for (int i = 0; i < N * N * N; i ++){
        phase[i] = pow(-1, i % 2);
       //cout << phase[i] << " ";
    }
    
    for (int i = N * N * N; i < 2 * N * N * N; i ++){
        phase[i] = pow(-1, (i / N) % 2);
       //cout << phase[i] << " ";
    }
    
    for (int i = 2 * N * N * N; i < Nb; i ++){
        phase[i] = pow(-1, (i / (N * N)) % 2);
       //cout << phase[i] << " ";
    }
   //cout << endl;
    
    /*for (int i = 0; i < N * N * N; i ++){ // links along the x-direction
        if (i % N == 0) {
            psi_x += phase[i] * (link[i] - link[i + N - 1]);
            //cout << i << " " << i + N - 1 << endl;
        } else {
            psi_x += phase[i] * (link[i] - link[i - 1]);
            //cout << i << " " << i - 1 << endl;
        }
    }
    
    for (int i = Nb / 3; i < Nb * 2 / 3; i ++) { // along the y-direction
        if ((i % (N * N)) / N == 0) {
            psi_y += phase[i] * (link[i] - link[i + N * (N - 1)]);
            //cout << i << " " << i + N * (N - 1) << endl;
        } else {
            psi_y += phase[i] * (link[i] - link[i - N]);
            //cout << i << " " << i - N << endl;
        }
    }
    
    for (int i = Nb * 2 / 3; i < Nb; i ++) { // z-direction
        if (i >= Nb * 2 / 3 && i < Nb * 2 / 3 + N * N) {
            psi_z += phase[i] * (link[i] - link[i + N * N * N - N * N]);
            //cout << i << " " << i + N * N * N - N * N << endl;
        } else {
            psi_z += phase[i] * (link[i] - link[i - N * N]);
            //cout << i << " " << i - N * N << endl;
        }
    }
    
    avg_x += psi_x / (1.0 * N * N * N);
    avg_y += psi_y / (1.0 * N * N * N);
    avg_z += psi_z / (1.0 * N * N * N);
    avg_r += sqrt(1.0 * psi_x * psi_x + 1.0 * psi_y * psi_y + 1.0 * psi_z * psi_z) / (1.0 * N * N * N);
    cout << avg_x << " " << avg_y << " " << avg_z << " " << avg_r << endl;
    avg_x = 0;
    avg_y = 0;
    avg_z = 0;
    avg_r = 0;*/
    
    // rotate!
    /*p = N * N;
    q = N * N + N - 1;
    link[p] = 1;
    link[q] = 1;
    for (u = p - N * N; u <= q - N * N - 1; u ++){
        link[u] = 1 - link[u];
    }
    if (p / N == 2 * N - 1){ // to deal with the rightmost column...
        for (u = p - N * N - N * (N - 1); u <= q - N * N - N * (N - 1) - 1; u ++){
            link[u] = 1 - link[u];
        }
    }
    else{
        for (u = p - N * N + N; u <= q - N * N + N - 1; u ++){
            link[u] = 1 - link[u];
        }
    }*/
    
    // copy the configuration to the 0th array
    for (u = 0; u < Nb; u ++){
        link0[u] = link[u];
        link0new[u] = link[u];
        
    }
   //cout << endl;
    
    // start the update
    for (int bin = 0; bin < 100; bin ++){
    //cout << "bin = " << bin << endl;
    for (int MC_sweep = 0; MC_sweep < MC_step; MC_sweep ++){
        cout << "MC sweep = " << MC_sweep + MC_step * bin << endl;
        //cout << "n = " << n << endl;
        q = 0;
        for (u = 0; u < Nb; u ++){
            if (link[u] == 1){
                q ++;
            }
        }
        if (q != Nb / 6){
            for (u = 0; u < 100; u ++){
               cout << "error" << endl;
                
            }
        }
       //cout << "n = " << n << endl;
        // diagonal update
       //cout << "diagonal update" << endl;
       //cout << "initial state" << endl;
        /*for (u = 0; u < Nb; u ++){
         if (link[u] == 1){
        //cout << u << " ";
         }
         }*/
       //cout << endl;
        for (int p = 0; p < M; p ++){
           //cout << "p = " << p << endl;
            if (s[p] == -1){
               //cout << "no operator" << endl;
                b = floor(Nb * conti(generate)); // choose a plaquette
               //cout << "b = " << b << endl;
                if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                   //cout << "flippable" << endl;
                    if (conti(generate) < N * N * beta / (M - n)){
                       //cout << "insert an FP" << endl;
                        s[p] = 2 * b;
                       //cout << "s" << p << " = " << s[p] << endl;
                        n += 1;
                    }
                   //cout << "n = " << n << endl;
                }
                else{
                    if (conti(generate) < N * N * beta * (1 + V) / (M - n)){
                        s[p] = 2 * b;
                       //cout << "s" << p << " = " << s[p] << endl;
                        n += 1;
                       //cout << "n = " << n << endl;
                    }
                }
            }
            else if (s[p] % 2 == 0){
                b = s[p] / 2;
                if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                    if (conti(generate) < (M - n + 1) / (N * N * beta)){
                        s[p] = -1;
                       //cout << "s" << p << " = " << s[p] << endl;
                        n -= 1;
                    }
                   //cout << "n = " << n << endl;
                }
                else{
                    if (conti(generate) < (M - n + 1) / (N * N * beta * (1 + V))){
                        s[p] = -1;
                       //cout << "s" << p << " = " << s[p] << endl;
                        n -= 1;
                       //cout << "n = " << n << endl;
                    }
                }
            }
            else{
                b = s[p] / 2;
                link[i[b]] = 1 - link[i[b]];
                link[j[b]] = 1 - link[j[b]];
                link[k[b]] = 1 - link[k[b]];
                link[l[b]] = 1 - link[l[b]];
                
            }
            s_new[p] = s[p];
        }
        
       //cout << "operator configuration after diagonal update" << endl;
        for (p = 0; p < M; p ++){
           //cout << s[p] << " ";
        }
       //cout << endl;
        
        // off-diagonal update
        
       //cout << "off-diagonal update" << endl;
        for (int i = 0; i < Nb; i ++){
            if (link[i] != link0[i]){
               cout << "1error" << endl;
                
            }
        }
        // count the number of flippable plaquette
        for (int p = 0; p < M; p ++){
            if (s[p] != -1){
                b = s[p] / 2;
                if (s[p] % 2 == 1){
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                }
                if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                    num_FP ++;
                }
            }
        }
        
       //cout << "num of FP = " << num_FP << endl;
        while (a == 0){
            
            // find the starting stack
            
            prob = floor(num_FP * conti(generate));
           //cout << "prob = "  << prob << endl;
            for (int i = 0; i < Nb; i ++){
                updating_line[i] = 0;
            }
            int q = 0;
            int p = 0;
           //cout << "p = 0" << endl;
            while (q <= prob) {
                if (s[p] != -1){
                    b = s[p] / 2;
                    if (s[p] % 2 == 1){
                       //cout << "s[p] = " << s[p] << endl;
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                    }
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                       //cout << "q = " << q << endl;
                        q += 1; // we pick the prob-th flippable plaquette...
                    }
                }
                p += 1; // which is located in the p-th time stack
               //cout << "p = " << p << endl;
                if (p > M){
                   cout << "3error" << endl;
                }
                
                if (p > M - 1){
                    continue;
                }
            }
            
            p -= 1;
            q = p; // record where we start
           //cout << "q = " << q << endl;
            
            
            if (s[q] % 2 == 1){
                b = s[q] / 2;
                link[i[b]] = 1 - link[i[b]];
                link[j[b]] = 1 - link[j[b]];
                link[k[b]] = 1 - link[k[b]];
                link[l[b]] = 1 - link[l[b]];
            }
            // copy the configuration to the new array
            for (int i = 0; i < Nb; i ++){
                link_new[i] = link[i];
            }
            
            // start the update
            b = s[p] / 2;
           //cout << "b = " << b << endl;
            // first the old ones...
            if (s[p] % 2 == 1){
                link[i[b]] = 1 - link[i[b]];
                link[j[b]] = 1 - link[j[b]];
                link[k[b]] = 1 - link[k[b]];
                link[l[b]] = 1 - link[l[b]];
                
            }
            for (int u = 0; u < Nb; u ++){
                if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                    cout << "error!" << endl;
                    for (int i = 0; i < Nb; i ++){
                        cout << link[i] << " ";
                    }
                }
            }
            
            // and then the new ones...
            updating_line[i[b]] = 1;
            updating_line[j[b]] = 1;
            updating_line[k[b]] = 1;
            updating_line[l[b]] = 1;
            if (s_new[p] % 2 == 0){
                s_new[p] += 1; // flip the type of the operator
                link_new[i[b]] = 1 - link_new[i[b]];
                link_new[j[b]] = 1 - link_new[j[b]];
                link_new[k[b]] = 1 - link_new[k[b]];
                link_new[l[b]] = 1 - link_new[l[b]];
            }
            else{
                s_new[p] -= 1;
            }
            for (int u = 0; u < Nb; u ++){
                if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                    cout << "error!!" << endl;
                }
            }
            
           //cout << "s_new[p] = "<< s_new[p] << endl;
            p = (p + 1) % M;
            
            while (true){
               //cout << "p = " << p << endl;
                // AN ADDITIONAL REQUIREMENT: if the updating lines complete the whole imaginary time period
                if (p == q){
                    cout << "too long" << endl;
                    for (int i = 0; i < M; i ++){
                        s_new[i] = s[i];
                    }
                    for (int i = 0; i < Nb; i ++){
                        link0new[i] = link0[i];
                        link[i] = link0[i];
                    }
                    num_updating_line = 0; // annihilate all the updating lines
                    break; // we still don't have a complete cluster and thus need to initiate another one (the former one is abandoned)
                }
               //cout << "s[p] = " << s[p] << endl;
                // first the old ones
                if (s[p] % 2 == 1){
                    b = s[p] / 2;
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                    
                }
                for (int u = 0; u < Nb; u ++){
                    if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                        cout << "error!!!" << endl;
                    }
                }
                // then the new ones
                if (p == 0){
                    for (int i = 0; i < Nb; i ++){
                        link0new[i] = link_new[i];
                    }
                }
                
               //cout << endl;
                if (s_new[p] == -1){
                   //cout << "continue" << endl;
                    p = (p + 1) % M;
                    continue;
                }
                b = s_new[p] / 2;
               //cout << "b = " << b << endl;
                if (updating_line[i[b]] + updating_line[j[b]] + updating_line[k[b]] + updating_line[l[b]] == 0){
                    if (s_new[p] % 2 == 1){
                        link_new[i[b]] = 1 - link_new[i[b]];
                        link_new[j[b]] = 1 - link_new[j[b]];
                        link_new[k[b]] = 1 - link_new[k[b]];
                        link_new[l[b]] = 1 - link_new[l[b]];
                    }
                    for (int u = 0; u < Nb; u ++){
                        if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                            cout << "error!!!!" << endl;
                        }
                    }
                    p = (p + 1)% M;
                   //cout << "continue" << endl;
                    continue;
                }
                
                // case 1
                if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] != 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){ // if it's acting on an NFP before the update
                   //cout << "case 1" << endl;
                    if (conti(generate) < 0.5){
                        if (s_new[p] % 2 == 1){
                            s_new[p] -= 1;
                        }
                        else{
                            s_new[p] += 1;
                            b = s[p] / 2;
                            link_new[i[b]] = 1 - link_new[i[b]];
                            link_new[j[b]] = 1 - link_new[j[b]];
                            link_new[k[b]] = 1 - link_new[k[b]];
                            link_new[l[b]] = 1 - link_new[l[b]];
                        }
                        for (int u = 0; u < Nb; u ++){
                            if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                cout << "error!!!!!" << endl;
                            }
                        }
                    }
                }
                
                // case 2
                if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] != 2){
                   //cout << "case 2" << endl;
                    if (s_new[p] % 2 == 1){
                        s_new[p] -= 1;
                    }
                }
                
                // case 3
                if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                   //cout << "case 3" << endl;
                    a = 0;
                    for (int i = 0; i < Nb; i ++){
                        if (updating_line[i] == 1){
                            a ++;
                        }
                    }
                    if (a == 4){
                        updating_line[i[b]] = 0;
                        updating_line[j[b]] = 0;
                        updating_line[k[b]] = 0;
                        updating_line[l[b]] = 0;
                        if (s_new[p] % 2 == 0){
                            s_new[p] += 1;
                           //cout << "s_new[p] = " << s_new[p] << endl;
                            link_new[i[b]] = 1 - link_new[i[b]];
                            link_new[j[b]] = 1 - link_new[j[b]];
                            link_new[k[b]] = 1 - link_new[k[b]];
                            link_new[l[b]] = 1 - link_new[l[b]];
                            for (int u = 0; u < Nb; u ++){
                                if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                    cout << "error!!!!!!" << endl;
                                }
                            }
                        }
                        else{
                            s_new[p] -= 1;
                           //cout << "s_new[p] = " << s_new[p] << endl;
                        }
                        a = 1; // now we have a complete close cluster
                       //cout << "end" << endl;
                        break;
                    }
                    else{
                        a = 0;
                        if (s_new[p] % 2 == 1){
                            link_new[i[b]] = 1 - link_new[i[b]];
                            link_new[j[b]] = 1 - link_new[j[b]];
                            link_new[k[b]] = 1 - link_new[k[b]];
                            link_new[l[b]] = 1 - link_new[l[b]];
                        }
                    }
                }
                if (link_new[i[b]] != link[i[b]]){
                    updating_line[i[b]] = 1;
                }
                else{
                    updating_line[i[b]] = 0;
                }
                if (link_new[j[b]] != link[j[b]]){
                    updating_line[j[b]] = 1;
                }
                else{
                    updating_line[j[b]] = 0;
                }
                if (link_new[k[b]] != link[k[b]]){
                    updating_line[k[b]] = 1;
                }
                else{
                    updating_line[k[b]] = 0;
                }
                if (link_new[l[b]] != link[l[b]]){
                    updating_line[l[b]] = 1;
                }
                else{
                    updating_line[l[b]] = 0;
                }
                p = (p + 1) % M;
            }
        }
        a = 0;
        
        // determine whether the update is accepted (in this part because we have to keep the time stack p where the update stops, all the dummy variables are replaced by u
        
        ////count the new number of FP's
        //first we should move back to the 0-th stack
        for (int i = 0; i < Nb; i ++){
            link_new[i] = link0new[i];
        }
        for (int p = 0; p < M; p ++){
            if (s_new[p] != -1){
                int b = s_new[p] / 2;
                if (s_new[p] % 2 == 1){
                    link_new[i[b]] = 1 - link_new[i[b]];
                    link_new[j[b]] = 1 - link_new[j[b]];
                    link_new[k[b]] = 1 - link_new[k[b]];
                    link_new[l[b]] = 1 - link_new[l[b]];
                }
                if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                    num_FP_after ++;
                }
            }
        }
        prob = conti(generate);
        /*cout << "FP = " << num_FP << endl;
        //cout << "new FP = " << num_FP_after << endl;
        //cout << "delta = " << num_FP_after - num_FP << endl;
        //cout << "prob = " << (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP)) << endl;*/
        if (prob < (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP))){ // accepted!!!
           //cout << "accept" << endl;
            for (int i = 0; i < M; i ++){
                s[i] = s_new[i];
            }
            for (int i = 0; i < Nb; i ++){
                link0[i] = link0new[i];
            }
            /*for (u = 0; u < M; u ++){
            //cout << s[u] << " ";
             }
            //cout << endl;*/
            
        }
        else{
           //cout << "reject " << endl;
            for (int i= 0; i < M; i ++){
                s_new[i] = s[i];
            }
           //cout << "link0new" << endl;
            for (int i = 0; i < Nb; i ++){
                link0new[i] = link0[i];
                
            }
        }
        // move back to the 0-th stack
        for (int i = 0; i < Nb; i++){
            link[i] = link0[i];
        }
       //cout << "the updated operator configuration" << endl;
            for (u = 0; u < M; u ++){
               //cout << s[u] << " ";
        }
        for (int p = 0; p < M; p ++){
            if (s[p] % 2 == 1){
               //cout << "p = " << p << endl;
                b = s[p] / 2;
                link[i[b]] = 1 - link[i[b]];
                link[j[b]] = 1 - link[j[b]];
                link[k[b]] = 1 - link[k[b]];
                link[l[b]] = 1 - link[l[b]];
            }
        }
        for (u = 0; u < Nb; u ++){
            if (link[u] != link0[u]){
                cout << "error!!!!!!!" << endl;
            }
        }
        
        num_FP = 0;
        num_FP_after = 0;
        
        // measurement
        
        // measuring heat capacity
        
        
        // measuring columnar parameter
        
        if (MC_sweep % 1 == 0){
            for (int p = 0; p < M; p ++){
                //cout << "p = " << p << endl;
                if (s[p] != -1 && s[p] % 2 == 1){
                    //cout << "OD!!!" << endl;
                    b = s[p] / 2;
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                    
                }
                for (int i = 0; i < N * N * N; i ++){ // links along the x-direction
                    if (i % N == 0) {
                        psi_x += phase[i] * (link[i] - link[i + N - 1]);
                        //cout << i << " " << i + N - 1 << endl;
                    } else {
                        psi_x += phase[i] * (link[i] - link[i - 1]);
                        //cout << i << " " << i - 1 << endl;
                    }
                }
                
                for (int i = Nb / 3; i < Nb * 2 / 3; i ++) { // along the y-direction
                    if ((i % (N * N)) / N == 0) {
                        psi_y += phase[i] * (link[i] - link[i + N * (N - 1)]);
                        //cout << i << " " << i + N * (N - 1) << endl;
                    } else {
                        psi_y += phase[i] * (link[i] - link[i - N]);
                        //cout << i << " " << i - N << endl;
                    }
                }
                
                for (int i = Nb * 2 / 3; i < Nb; i ++) { // z-direction
                    if (i >= Nb * 2 / 3 && i < Nb * 2 / 3 + N * N) {
                        psi_z += phase[i] * (link[i] - link[i + N * N * N - N * N]);
                        //cout << i << " " << i + N * N * N - N * N << endl;
                    } else {
                        psi_z += phase[i] * (link[i] - link[i - N * N]);
                        //cout << i << " " << i - N * N << endl;
                    }
                }
            }
            
            avg_x += psi_x / (1.0 * N * N * N * M);
            avg_y += psi_y / (1.0 * N * N * N * M);
            avg_z += psi_z / (1.0 * N * N * N * M);
            avg_r += sqrt(1.0 * psi_x * psi_x + 1.0 * psi_y * psi_y + 1.0 * psi_z * psi_z) / (1.0 * N * N * N * M);
            
            // compute the projection of anisotropy measure on the x-y plane
            r = sqrt(1.0 * psi_x / M * psi_x / M + 1.0 * psi_y / M * psi_y / M); // the norm of the projection of psi on the plane
            Cos = 1.0 * psi_y / M / r;
            //cout << "xy " << Cos << endl;
            //cout << 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0 << " ";
            if (fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) > 1.0) {
                cout << "Cos = " << Cos << endl;
                cout << "Cos 4x = " << fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) << endl;
                cout << "babu" << endl;
                
            }
            xy += r * 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0; // cos 4x
            
            // the x-z plane
            r = sqrt(1.0 * psi_x / M * psi_x / M + 1.0 * psi_z / M * psi_z / M); // same as above
            Cos = 1.0 * psi_z / M / r;
            //cout << "xz " << Cos << endl;
            //cout << 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0 << " ";
            if (fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) > 1.0) {
                cout << "Cos = " << Cos << endl;
                cout << "Cos 4x = " << fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) << endl;
                cout << "babu" << endl;
                
            }
            xz += r * 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0;
            
            // the y-z plane
            r = sqrt(1.0 * psi_y / M * psi_y / M + 1.0 * psi_z / M * psi_z / M);
            Cos = 1.0 * psi_y / M / r;
            //cout << "yz " << Cos << endl;
            //cout << 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0 << endl;
            if (fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) > 1.0) {
                cout << "Cos = " << Cos << endl;
                cout << "Cos 4x = " << fabs(8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0) << endl;
                cout << "babu" << endl;
                
            }
            yz += r * 8.0 * (Cos * Cos * Cos * Cos - Cos * Cos) + 1.0;
            
            psi_x = 0;
            psi_y = 0;
            psi_z = 0;
        }
        
        if ((float)n / M > 0.75){ // expansion
            M = ceil(4.0 / 3.0 * M);
            cout << "new M = " << M << endl;
        }
        
        //cout << "psi" << endl;
        //cout << n << endl;
        /*if (MC_sweep % 100 == 0){
        //cout << "for V = -0.5" << endl;
         for (u = 0; u < Nb; u ++){
        //cout << link[u] << " ";
         }
        //cout << endl;
         for (p = 0; p < M; p ++){
        //cout << s[p] << " ";
         }
        //cout << endl;
         }*/
    }
        avg_x /= MC_step / 1.0;
        avg_y /= MC_step / 1.0;
        avg_z /= MC_step / 1.0;
        avg_r /= MC_step / 1.0;
        cout << avg_x << " " << avg_y << " " << avg_z << " " << avg_r << " ";
        avg_x = 0;
        avg_y = 0;
        avg_z = 0;
        avg_r = 0;

        xy /= MC_step / 1.0;
        yz /= MC_step / 1.0;
        xz /= MC_step / 1.0;
        cout << xy << " " << xz << " " << yz << endl;
        xy = 0;
        xz = 0;
        yz = 0;
    }
}



